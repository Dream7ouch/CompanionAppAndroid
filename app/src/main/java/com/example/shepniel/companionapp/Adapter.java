package com.example.shepniel.companionapp;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by shepniel on 20/11/16.
 */
public class Adapter extends CursorAdapter {
    public Adapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.select_layout,viewGroup,false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView textView_Name = (TextView)view.findViewById(R.id.tvBody);
        String name = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        textView_Name.setText(name);
    }
}
