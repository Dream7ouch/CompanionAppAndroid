package com.example.shepniel.companionapp;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by shepniel on 21/11/16.
 */
public class AdapterOption extends BaseAdapter {

    private int icons[];
    private String letters[];
    private LayoutInflater inflater;
    Context context;

    public AdapterOption(Context context,int icons[],String letters[]){
        this.context=context;
        this.icons=icons;
        this.letters=letters;
    }

    @Override
    public int getCount() {
        return letters.length;
    }

    @Override
    public Object getItem(int position) {
        return letters[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View view1 = view;

        if (view == null){

            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view1 = inflater.inflate(R.layout.option_layout,null);
        }

        ImageView icon = (ImageView) view1.findViewById(R.id.imageView);
        TextView letter = (TextView) view1.findViewById(R.id.textView2);

        icon.setImageResource(icons[i]);
        letter.setText(letters[i]);

        return view1;
    }
}
