package com.example.shepniel.companionapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;


public class Network {
    public static final String URL = "https://companionappitchii.herokuapp.com";

    public static boolean isConnected(Context context) {
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static void signIn(Context context, JSONObject data, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = URL + "/users/sign_in";
        JRequest request = new JRequest(Request.Method.POST, url, data, listener, errorListener);
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        Volley.newRequestQueue(context).add(request);
    }

    public static void post(Context context, String service, JSONObject data, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = URL + service;
        JRequest request = new JRequest(Request.Method.POST, url, data, listener, errorListener);
        Volley.newRequestQueue(context).add(request);
    }

    public static void get(Context context, String service, JSONObject data, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = URL + service;
        JRequest request = new JRequest(Request.Method.GET, url, data, listener, errorListener);
        Volley.newRequestQueue(context).add(request);
    }

    static class JRequest extends JsonObjectRequest {
        public JRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
            super(method, url, jsonRequest, listener, errorListener);
        }

        @Override
        public String getBodyContentType() {
            return "application/json; charset=utf-8";
        }
    }
}
