package com.example.shepniel.companionapp;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

public class Option extends AppCompatActivity {

    SQLiteDatabase miBD;
    AdapterOption adapter1;
    GridView gridView;

    String letterList[] = {"Introduction","Diagramas de Flujo","Oriented Objective Programming"};

    int lettersIcons[] = {R.drawable.introduction,R.drawable.programming,R.drawable.poo};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_option);
        gridView = (GridView)findViewById(R.id.gridView);

        AdapterOption adapterOption = new AdapterOption(this,lettersIcons,letterList);
        gridView.setAdapter(adapterOption);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int id = i;
                Intent intro = new Intent(getApplicationContext(),Topic_Select.class);
                Intent intro2 = new Intent(getApplicationContext(),Topic_Select2.class);

                if (id==0){
                    startActivity(intro);
                }else if (id==1){
                    startActivity(intro2);
                }
            }
        });

    }


}
