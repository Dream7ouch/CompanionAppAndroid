package com.example.shepniel.companionapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class Register extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {

    EditText edtTxtEmail, edtTxtPassword;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        prefs = getSharedPreferences("user", MODE_PRIVATE);
        edtTxtEmail = (EditText) findViewById(R.id.edtTxtEmail);
        edtTxtPassword = (EditText) findViewById(R.id.edtTxtPassword);
    }

    public void register(View v) {
        if (Network.isConnected(this)) {
            try {
                JSONObject user = new JSONObject();
                user.put("email", edtTxtEmail.getText().toString());
                user.put("password", edtTxtPassword.getText().toString());
                JSONObject data = new JSONObject();
                data.put("user", user);
                Network.post(this, "/users", data, this, this);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "No hay conexión a internet.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.e("R", response.toString());
        try {
            if (response.has("errors")) {
                Toast.makeText(this, response.getString("errors"), Toast.LENGTH_SHORT).show();
            } else if (response.has("user")) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("logged", true);
                editor.putInt("id", response.getInt("user"));
                editor.apply();
                startActivity(new Intent(this, Select.class));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
