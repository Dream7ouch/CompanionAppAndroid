package com.example.shepniel.companionapp;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class Select extends AppCompatActivity {

    ListView listView;
    SQLiteDatabase BD;
    Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);
        listView = (ListView) findViewById(R.id.listView);
        BD = openOrCreateDatabase("DATA", MODE_PRIVATE, null);
        try {
            BD.execSQL("CREATE TABLE IF NOT EXISTS tblAmigo(" +
                    "_id integer PRIMARY KEY autoincrement," +
                    "name text);");
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        try {
            ContentValues myArgs = new ContentValues();
            myArgs.put("_id", 0);
            myArgs.put("name", "Topics");
            BD.insert("tblAmigo", null, myArgs);
            myArgs.put("_id", 1);
            myArgs.put("name", "Ayudas para estudiar");
            BD.insert("tblAmigo", null, myArgs);
            myArgs.put("_id", 2);
            myArgs.put("name", "Vota por el siguiente Tema");
            BD.insert("tblAmigo", null, myArgs);
            myArgs.put("_id", 3);
            myArgs.put("name", "About");
            BD.insert("tblAmigo", null, myArgs);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }

        final Cursor Cursor_list = BD.rawQuery("SELECT * FROM tblAmigo", null);
        adapter = new Adapter(this, Cursor_list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent Option = new Intent(getApplicationContext(), Option.class);
                Intent Help = new Intent(getApplicationContext(), Study_Help.class);
                Intent next = new Intent(getApplicationContext(), Suggest.class);
                Intent about = new Intent(getApplicationContext(), About.class);
                int position = i;
                if (position == 0) {
                    startActivity(Option);
                } else if (position == 1) {
                    startActivity(Help);
                } else if (position == 2) {
                    startActivity(next);
                } else if (position == 3) {
                    startActivity(about);
                }
            }
        });
    }
}
