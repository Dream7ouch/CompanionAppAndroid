package com.example.shepniel.companionapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class Suggest extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {

    RadioGroup rdGrpTemas;
    TextView enviado;
    Button btnEnviar;
    private int id;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest);
        rdGrpTemas = (RadioGroup) findViewById(R.id.rdGrpTemas);
        enviado = (TextView) findViewById(R.id.textView8);
        btnEnviar = (Button) findViewById(R.id.button);
        prefs = getSharedPreferences("user", MODE_PRIVATE);
        if (prefs.getBoolean("vote", false)) {
            rdGrpTemas.setVisibility(View.INVISIBLE);
            enviado.setVisibility(View.VISIBLE);
            btnEnviar.setVisibility(View.INVISIBLE);
        }
    }

    public void vote(View v) {
        try {
            JSONObject voto = new JSONObject();
            switch (rdGrpTemas.getCheckedRadioButtonId()) {
                case R.id.rdBtnWeb:
                    id = 1;
                    break;
                case R.id.rdBtnAndroid:
                    id = 2;
                    break;
                case R.id.rdBtnIOS:
                    id = 3;
                    break;
                case R.id.rdBtnHibridas:
                    id = 4;
                    break;
                case R.id.rdBtnPOO:
                    id = 5;
                    break;
            }
            voto.put("language_id", id);
            Network.post(this, "/language_votes/plus_vote", voto, this, this);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.e("v", response.toString());
        try {
            if (response.has("errors")) {
                Toast.makeText(this, response.getString("errors"), Toast.LENGTH_SHORT).show();
            } else if (response.has("id")) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("vote", true);
                editor.apply();
                Toast.makeText(this, "Se envio tu voto", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), Select.class));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
